import { createApp } from 'vue'
import App from './App.vue'
import firebase from "firebase"

// Required for side-effects
require("firebase/firestore");
const config = {
    apiKey: "AIzaSyBW3dZ-_dmea24517e3bo1z_x2XOB8k6Yk",
    authDomain: "chat-93ce9.firebaseapp.com",
    projectId: "chat-93ce9",
    storageBucket: "chat-93ce9.appspot.com",
    messagingSenderId: "821909732809",
    appId: "1:821909732809:web:bffd6803eb7eddd4e4fc18",
    measurementId: "G-9PH9BFGLBG"
  };

  const db = firebase.firestore()

  window.db = db

  db.setting({
    timestampsInSnapshots: true
  })

  

firebase.initializeApp(config)
  
 
createApp(App).mount('#app')
